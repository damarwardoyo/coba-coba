from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
response = {}
def index(request):
		response['author'] = "Damar Wardoyo"
		html = 'lab_6/lab_6.html'
		return render(request, html, response)