-publish_actions

Provides access to publish Posts, Open Graph actions, and other activity on behalf of a person using your app.

-user_posts

Provides access to the posts on a person's Timeline. Includes their own posts, posts they are tagged in,
and posts other people make on their Timeline.

-callback

Callback hanyalah sebuah nama teknik dalam menggunakan function. Callback bukan lah fitur khusus dari JavaScript,
tapi lebih kepada konvensi bagaimana menggunakan function untuk menangani proses asynchronous.
Tidak seperti function pada umumnya yang segera menghasilkan nilai balik, function yang menggunakan callback akan membutuhkan waktu
beberapa saat untuk menghasilkan nilai. Salah satu pustaka pemrograman JavaScript yang banyak menggunakan callback adalah jQuery.
